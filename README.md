# Slicing tokopedia

Live Site: [slicing-tokopedia.vercel.app](https://slicing-tokopedia.vercel.app/)

Created from scratch. A tokopedia homepage clone using Next.js and Tailwind. Auto deploy to vercel if any new push to master branch

## Task List

- [x] Page Styling
- [x] Responsive
- [x] Navbar
- [x] Sticky Nav when scrolling
- [x] Product list
- [x] Loadmore product
- [x] Add product to cart
- [x] Remove product from cart
- [x] PPOB form
- [x] Swipe slider
- [x] Modal Login
- [x] Register Page
- [x] Flash sale product
- [x] Countdown
- [ ] All task done..?

> Let me know for other functionality :D thanks.

### Current build output

```sh
Page                              Size     First Load JS
┌ ● / (530 ms)                    41.9 kB        86.5 kB
├   /_app                         0 B            42.8 kB
├ ○ /404                          194 B            43 kB
├ λ /api/dummy                    0 B            42.8 kB
├ ○ /register                     3.13 kB        47.7 kB
└ ○ /testpage                     288 B          43.1 kB
+ First Load JS shared by all     42.8 kB
  ├ chunks/framework.ba7399.js    522 B
  ├ chunks/main.325a04.js         31 kB
  ├ chunks/pages/_app.08f73b.js   10.5 kB
  ├ chunks/webpack.f47d69.js      773 B
  └ css/fdf11f4b017bacdd5572.css  11.9 kB

λ  (Server)  server-side renders at runtime (uses getInitialProps or getServerSideProps)
○  (Static)  automatically rendered as static HTML (uses no initial props)
●  (SSG)     automatically generated as static HTML + JSON (uses getStaticProps)
   (ISR)     incremental static regeneration (uses revalidate in getStaticProps)

Done in 33.70s.
```

### Links

[My Website](https://arisris.com)

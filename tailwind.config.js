const { spacing, fontFamily } = require('tailwindcss/defaultTheme');
const screenSize = require('./libs/screenSize');
const screens = Object.keys(screenSize)
  .map((i) => {
    return { k: i, v: screenSize[i] + 'px' };
  })
  .reduce((a, b) => {
    a[b.k] = b.v;
    return a;
  }, {});
module.exports = {
  purge: [
    './libs/**/*.js',
    './components/**/*.js',
    './pages/**/*.js',
    './hooks/**/*.js'
  ],
  mode: 'jit',
  darkMode: false,
  theme: {
    screens,
    extend: {}
  },
  daisyui: {
    themes: [
      {
        mytheme: {
          primary: '#03ac0e',
          'primary-focus': '#069d10',
          'primary-content': '#ffffff',
          secondary: '#f000b8',
          'secondary-focus': '#bd0091',
          'secondary-content': '#ffffff',
          accent: '#37cdbe',
          'accent-focus': '#2aa79b',
          'accent-content': '#ffffff',
          neutral: '#3d4451',
          'neutral-focus': '#2a2e37',
          'neutral-content': '#ffffff',
          'base-100': '#ffffff',
          'base-200': '#f9fafb',
          'base-300': '#d1d5db',
          'base-content': '#1f2937',
          info: '#2094f3',
          success: '#009485',
          warning: '#ff9900',
          error: '#ff5724'
        }
      }
    ]
  },
  variants: {},
  plugins: [
    require('@tailwindcss/typography'),
    require('tailwindcss-animatecss')({
      classes: [
        'animate__animated',
        'animate__fadeIn',
        'animate__zoomIn',
        'animate__fast',
        'animate__faster'
      ],
      settings: {
        animatedSpeed: 300,
        heartBeatSpeed: 300,
        hingeSpeed: 750,
        bounceInSpeed: 450,
        bounceOutSpeed: 450,
        animationDelaySpeed: 300
      },
      variants: ['responsive', 'hover']
    }),
    require('daisyui')
  ]
};

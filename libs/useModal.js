import { useState, useRef, useEffect } from 'react';
import { Portal } from 'react-portal';
import clsx from 'clsx';

export default function useModal(open = false) {
  if (typeof open !== 'boolean') throw new Error('Invalid modal arguments');
  const [isOpen, setOpen] = useState(open);
  const toggle = () => setOpen(!isOpen);
  useEffect(() => {
    if (isOpen) {
      document.body.classList.add('overflow-hidden');
    } else {
      document.body.classList.remove('overflow-hidden');
    }
  }, [isOpen]);
  const Component = ({ children, className, closeable }) => {
    return (
      isOpen && (
        <Portal node={process.browser && document.body}>
          <div
            className={clsx('modal', {
              'modal-open': isOpen
            })}
          >
            <div
              className={clsx('modal-box', {
                className
              })}
            >
              {children}
              <div className="modal-action">
                <button
                  className="btn btn-sm"
                  onClick={(e) => {
                    e.preventDefault();
                    toggle();
                  }}
                >
                  Close
                </button>
              </div>
            </div>
          </div>
        </Portal>
      )
    );
  };
  return {
    isOpen,
    toggle,
    Component
  };
}

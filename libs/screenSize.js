module.exports = {
  xxxxs: 260,
  xxxs: 319,
  xxs: 359,
  xs: 410,
  sm: 640,
  md: 768,
  lg: 1024,
  xl: 1280,
  '2xl': 1536
};

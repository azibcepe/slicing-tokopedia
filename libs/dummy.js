import mock_data from '@/data/mock_data.json';
import { paginateArray } from '@/libs/utils';
const reversed_mock = mock_data.reverse();

// paginated dummy data without description
export function getPaginated(page = 1, perPage = 18) {
  const paginated = paginateArray(reversed_mock, page, perPage);
  if (paginated.data.length) {
    paginated.data = paginated.data.map((i) => {
      delete i.description;
      return i;
    });
  }
  return paginated;
}

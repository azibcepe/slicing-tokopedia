export function fakeArray(num = 10) {
  return Array(num)
    .fill(null)
    .map((i) => i);
}
export const preventDefault = (e) => e.preventDefault();
export const random = function() {
  return Math.floor(Math.random() * Date.now()).toString(36);
};
export const randomNumber = function(max) {
  const RANDOM_NUMBER = '0123456789';
  var builder = '';
  for (var i = 0; i < max; i++) {
    var index = Math.floor(Math.random() * RANDOM_NUMBER.length);
    if (!i && !index) index++;
    builder += RANDOM_NUMBER[index];
  }
  return builder;
};

export const GUID = function(max) {
  max = max || 40;
  var str = '';
  for (var i = 0; i < max / 3 + 1; i++) str += random();
  return str.substring(0, max);
};
// https://stackoverflow.com/a/6274398
export const randomizeArrayIndex = (arr) =>
  arr.filter((i) => i)[Math.floor(Math.random() * arr.length)];
export function paginateArray(collection, page = 1, numItems = 10) {
  if (!Array.isArray(collection)) {
    throw `collection accept array. but got ${typeof collection}`;
  }
  const currentPage = parseInt(page);
  const perPage = parseInt(numItems);
  const offset = (page - 1) * perPage;
  const paginatedItems = collection.slice(offset, offset + perPage);

  return {
    currentPage,
    perPage,
    total: collection.length,
    totalPages: Math.ceil(collection.length / perPage),
    data: paginatedItems
  };
}

export const formatAngka = (n, p = 1) => {
  let angka = parseInt(n);
  return angka.toLocaleString('id-ID', {
    maximumSignificantDigits: 3
  });
};


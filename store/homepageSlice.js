import { createSlice } from '@reduxjs/toolkit';

const homepageSclice = createSlice({
  name: 'homepage',
  initialState: {
    category: null
  },
  reducers: {
    setCategory(state, action) {
      state.category = action.payload;
    }
  }
});

export const { setCategory } = homepageSclice.actions;

export default homepageSclice.reducer;

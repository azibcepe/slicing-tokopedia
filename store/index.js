import { configureStore } from '@reduxjs/toolkit';
import { setupListeners } from '@reduxjs/toolkit/query/react';
import cart from './cart';
import { productApi } from './productApi';
import homepage from './homepageSlice'

// Ohhhhhh. Setelah sekian lama. Saya baru pertama kali menyentuh redux :D

export const store = configureStore({
  reducer: {
    homepage,
    cart,
    [productApi.reducerPath]: productApi.reducer
  },
  middleware: (m) => m().concat(productApi.middleware)
});
setupListeners(store.dispatch);

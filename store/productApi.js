import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

export const productApi = createApi({
  reducerPath: 'productApi',
  baseQuery: fetchBaseQuery({ baseUrl: 'https://fakestoreapi.com/' }),
  endpoints: (builder) => ({
    getProducts: builder.query({
      query: (options = {}) => {
        return `products${options?.category ? '/category/' + options.category : ''}?limit=${options?.limit ||
          18}&sort=${options?.sort || 'desc'}`
      }
    }),
    getCategories: builder.query({
      query: () => `products/categories`
    }),
    getDetail: builder.query({
      query: (id) => `products/${id}`
    })
  })
});

export const {
  useGetProductsQuery,
  useGetCategoriesQuery,
  useGetDetailQuery
} = productApi;

import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import gs from 'good-storage';

const LOCAL_KEY = 'app_cart_list';
const cartSlice = createSlice({
  name: 'cart',
  initialState: {
    items: gs.get(LOCAL_KEY, [])
  },
  reducers: {
    addCart(state, action) {
      if (typeof action.payload !== 'object') return state;
      if (action.payload.id) {
        state.items.push(action.payload);
        gs.set(LOCAL_KEY, state.items);
      }
    },
    removeCart(state, action) {
      if (typeof action.payload !== 'number') return state;
      state.items = state.items.filter((i) => i.id !== action.payload);
      gs.set(LOCAL_KEY, state.items);
      return state;
    }
  }
});

export const { addCart, removeCart } = cartSlice.actions;
export default cartSlice.reducer;

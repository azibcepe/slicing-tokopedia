import { getPaginated } from '@/libs/dummy.js';
export default function dummyApi(req, res) {
  res.json(
    getPaginated(parseInt(req.query.page) || 1, parseInt(req.query.perPage) || 18)
  );
}

import FrontLayout from '@/components/FrontLayout';
import { useRouter } from 'next/router';
import { useGetDetailQuery } from '@/store/productApi';
import Link from 'next/link';
import clsx from 'clsx';
import { FaStar } from 'react-icons/fa';
import Belanja from '@/components/Belanja';

function BreadCrumb({ list }) {
  return (
    <div className="capitalize text-sm breadcrumbs">
      <ul>
        {list.map((i, index) => (
          <li key={index}>
            <Link href={i.link}>
              <a className="">{i.title}</a>
            </Link>
          </li>
        ))}
      </ul>
    </div>
  );
}

function ProductDetail({ data }) {
  const breadcrumb = [
    {
      link: '/',
      title: 'Home'
    },
    {
      link: `#${data.category}`,
      title: data.category
    },
    {
      link: `/details?id=${data.id}`,
      title: data.title,
      truncated: true
    }
  ];
  return (
    <>
      <BreadCrumb list={breadcrumb} />
      <div className="flex flex-col md:flex-row my-20 gap-4">
        <div className="w-full md:w-4/12">
          <div className="max-w-[300px] m-auto">
            <img src={data.image} />
          </div>
        </div>
        <div className="w-full md:w-5/12">
          <h1 className="text-2xl font-black">{data.title}</h1>
          <ul className="inline-flex gap-4">
            <li>
              <span>Terjual </span>
              <span className="text-gray-500">{4 * 12}</span>
            </li>
            <li className="inline-flex items-center gap-1">
              <span>
                <FaStar className="text-yellow-500 w-5 h-5" />
              </span>
              <span>{data.rating.rate} /</span>
              <span className="text-gray-500">
                ({data.rating.count}) Ulasan
              </span>
            </li>
            <li>
              <span>Diskusi </span> <span className="text-gray-500">(20)</span>
            </li>
          </ul>
          <div className="my-5">
            <strong className="text-2xl">$ {data.price}</strong>
          </div>
          <nav className="tabs">
            <a tabIndex="0" className="tab tab-md tab-lifted tab-active">
              Detail
            </a>
            <a tabIndex="1" className="tab tab-md tab-lifted">
              Informasi Penting
            </a>
          </nav>
          <div className="mt-5">
            <ul className="mb-5">
              <li>
                <span className="text-gray-500">Kondisi: </span>
                <span>Baru</span>
              </li>
              <li>
                <span className="text-gray-500">Berat: </span>
                <span>1kg</span>
              </li>
              <li>
                <span className="text-gray-500">Kategori: </span>
                <Link href={`#category${data.category}`}>
                  <a className="capitalize text-primary font-black">
                    {data.category}
                  </a>
                </Link>
              </li>
              <li>
                <span className="text-gray-500">Etalase: </span>
                <Link href={`#category${data.category}`}>
                  <a className="capitalize text-primary font-black">
                    Lain-lain
                  </a>
                </Link>
              </li>
            </ul>
            <div className="prose">{data.description}</div>
          </div>
        </div>
        <div className="w-full md:w-3/12">
          <div className="card rounded shadow">
            <div className="card-body p-4">
              <h3 className="card-title">Keranjang</h3>
              <Belanja data={data} />
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default function PageDetails() {
  const router = useRouter();
  const { data, isLoading, isError } = useGetDetailQuery(router.query.id);
  return (
    <FrontLayout>
      {isLoading ? (
        <div>Loading data</div>
      ) : isError ? (
        <div>Error while loading data</div>
      ) : (
        <ProductDetail data={data} />
      )}
    </FrontLayout>
  );
}

import '@/styles/global.css';
import { store } from '@/store/index';
import { Provider as ReactReduxProvider } from 'react-redux';
import { configResponsive } from 'ahooks';
import screen from '@/libs/screenSize';

configResponsive(screen);

function App({ Component, pageProps }) {
  return (
    <ReactReduxProvider store={store}>
      <Component {...pageProps} />
    </ReactReduxProvider>
  );
}
export default App;

import Link from 'next/link';
import ModalLogin from '@/components/ModalLogin';
import Head from 'next/head';

export default function PageRegister() {
  return (
    <div className="absolute flex flex-col items-center w-full h-full p-4 md:p-8">
      <Head>
        <title>Daftar | Toko Alim Rugi</title>
      </Head>
      <Link href="/">
        <a>
          <img src="/images/tokopedia.svg" />
        </a>
      </Link>
      <div className="max-w-screen-lg grid grid-cols-1 lg:grid-cols-2 gap-20 items-center py-20">
        <div className="text-center">
          <figure>
            <img src="/images/register_new.png" />
          </figure>
          <h1 className="mt-2 text-xl font-bold">Jual Beli Mudah Hanya Di Toko Alim Rugi</h1>
          <p>Gabung dan rasakan kemudahan bertransaksi di Alim Rugi :D .</p>
        </div>
        <div className="border rounded-md shadow-md p-4">
          <ModalLogin />
        </div>
      </div>
      <div>
        &copy; 2021 PT. Alim Rugi
      </div>
    </div>
  );
}

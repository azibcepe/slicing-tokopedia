import FrontLayout from '@/components/FrontLayout';
import Carousel from '@/components/Carousel';
import CategoryAndPPOB from '@/components/CategoryAndPPOB';
import FlashSale from '@/components/FlashSale';
import { preventDefault } from '@/libs/utils';
import Products from '@/components/Products';
import { getPaginated } from '@/libs/dummy';
import fs from 'fs';
import path from 'path';

export default function IndexPage({ carousel, categories, products }) {
  return (
    <FrontLayout>
      <Carousel
        items={carousel.map((item, index) => (
          <a
            onClick={preventDefault}
            key={index}
            href={item.link}
            className="flex items-center"
          >
            <img src={item.image} className="object-fill w-full" />
          </a>
        ))}
        autoplay
        pagination
        navigation
      />
      <CategoryAndPPOB />
      <FlashSale />
      <Products />
    </FrontLayout>
  );
}

export async function getStaticProps() {
  const carousel = JSON.parse(
    fs.readFileSync(path.join(process.cwd(), '/data/carousel.json'), 'utf-8')
  );
  // const categories = JSON.parse(
  //   fs.readFileSync(path.join(process.cwd(), '/data/categories.json'), 'utf-8')
  // );
  return {
    props: { carousel, /*categories, products: getPaginated()*/ }
  };
}

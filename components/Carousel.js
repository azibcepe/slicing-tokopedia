import { useState } from 'react';
import Image from 'next/image';
import { Swiper, SwiperSlide } from 'swiper/react';
import SwiperCore, { Navigation, Pagination, Autoplay } from 'swiper';

export default function Carousel({ items, children, className, ...props }) {
  return (
    <Swiper
      modules={[Navigation, Autoplay, Pagination]}
      className="z-0"
      {...props}
    >
      {items.map((item, index) => {
        return (
          <SwiperSlide key={index}>
            {item}
          </SwiperSlide>
        );
      })}
    </Swiper>
  );
}

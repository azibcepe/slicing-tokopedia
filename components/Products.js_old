import { useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { useSetState } from 'ahooks';
import ProductCard from '@/components/ProductCard';
import NavTag from '@/components/NavTag';
// fetch products
const fetchProducts = async (page = 1, perPage = 18) =>
  await (await fetch(`/api/dummy?page=${page}&perPage=${perPage}`)).json();

function Products({
  initial = {
    data: [],
    currentPage: 1
  }
}) {
  const loadMoreRef = useRef();
  const [status, setStatus] = useSetState({ loading: false, error: null });
  const [products, setProducts] = useSetState({
    data: initial.data,
    currentPage: initial.currentPage
  });

  const dispatchData = (onData, page) => {
    setStatus({ loading: true, error: null });
    fetchProducts(page)
      .then(onData)
      .catch((e) => setStatus({ error: e.message }))
      .finally(() => setStatus({ loading: false }));
  };

  const loadMoreProducts = () => {
    dispatchData((res) => {
      setProducts((s) => ({
        currentPage: res.currentPage,
        data: s.data.concat(res.data)
      }));
    }, products.currentPage + 1);
  };

  useEffect(() => {
    // trigger if no initial data found at props
    if (products.data.length === 0) {
      // start at page one
      dispatchData(setProducts, 1);
    }
  }, []);

  return initial.data.length !== 0 || products.data.length !== 0 ? (
    <>
      <div className="my-5">
        <NavTag />
      </div>
      <div className="mt-6 p-2 lg:p-0">
        <div className="grid grid-cols-2 sm:grid-cols-3 md:grid-cols-4 lg:grid-cols-5 xl:grid-cols-6 gap-2">
          {products.data.map((i, index) => (
            <ProductCard id={`index${i}`} key={i.id} data={i} />
          ))}
        </div>
      </div>
      <div ref={loadMoreRef} className="text-center mt-6">
        <a
          href="#"
          className="btn btn-primary btn-md btn-outline capitalize px-20"
          onClick={(e) => {
            e.preventDefault();
            loadMoreRef.current.scrollIntoView({
              behavior: 'smooth',
              block: 'start',
              inline: 'nearest'
            });
            loadMoreProducts();
          }}
        >
          Muat Lebih Banyak
        </a>
      </div>
    </>
  ) : (
    <div className="p-4">Empty Data</div>
  );
}

Products.propTypes = {
  initial: PropTypes.shape({
    currentPage: PropTypes.number,
    data: PropTypes.array
  })
};
export default Products;

import { useDispatch, useSelector } from 'react-redux';
import Link from 'next/link';
import { addCart, removeCart } from '@/store/cart';
import { preventDefault } from '@/libs/utils';
import { FaStar, FaShoppingCart } from 'react-icons/fa';
import { FcApproval, FcApprove, FcBusinessman } from 'react-icons/fc';
import { formatAngka } from '@/libs/utils';

export default function ProductCard({ data }) {
  const cart = useSelector((state) => state.cart);
  const dispatch = useDispatch();

  return (
    <div
      id={data.id}
      className="card rounded-md shadow border cursor-pointer bg-white"
    >
      <Link href={`/details?id=${data.id}`}>
        <a className="w-full">
          <img src={data.image} className="w-48 h-48 rounded-t-md" />
          {data.rating.count > 300 && (
            <div className="whitespace-nowrap text-white font-bold text-xs text-center w-14 pr-2 pt-[2px] h-5 bg-yellow-500 rounded-l rounded-full">
              Terlaris
            </div>
          )}
        </a>
      </Link>
      <div className="relative card-body p-2 gap-y-1">
        <Link href={`/details?id=${data.id}`}>
          <a className="hover:underline">
            <h3 className="text-sm">{data.title}</h3>
          </a>
        </Link>
        <div className="inline-flex justify-between items-center">
          <div className="font-black">{data.price}</div>
          <button
            type="button"
            className="btn btn-sm btn-ghost btn-circle text-green-800"
            onClick={(e) => {
              e.preventDefault();
              dispatch(addCart(data));
            }}
          >
            <FaShoppingCart className="w-4 h-4" />
          </button>
        </div>
        <div className="inline-flex items-center text-[13px] text-gray-500 gap-1 h-6">
          <span className="inline-flex text-center items-baseline">
            <FaStar className="w-4 mr-[1px] text-yellow-500" />
            {data.rating.rate} / {data.rating.count} Ulasan
          </span>
          {/*<hr className="w-[1px] h-4 bg-gray-300" />
          <span>Terjual {formatAngka(data.rating.count)}</span>*/}
        </div>
      </div>
    </div>
  );
}

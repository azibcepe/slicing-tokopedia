import { useRef, useEffect } from 'react';
import Navbar from './Navbar';
import Head from 'next/head';
import Footer from '@/components/Footer';
import { useScroll } from 'ahooks';

export default function FrontLayout({ children, title, description }) {
  const headerRef = useRef();
  const scroll = useScroll();
  useEffect(() => {
    let h = headerRef.current;
    if (h) {
      if (scroll.top > h.scrollHeight) {
        if (!h.classList.contains('sticky-nav')) {
          h.classList.add('sticky-nav');
        }
      } else {
        if (h.classList.contains('sticky-nav')) {
          h.classList.remove('sticky-nav');
        }
      }
    }
  }, [scroll]);
  return (
    <>
      <Head>
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0"
        />
        <title>{title || 'Toko Alim Rugi'}</title>
      </Head>
      <section className="absolute flex flex-col w-full h-full">
        <header ref={headerRef} className="bg-white block mb-4">
          <Navbar />
        </header>
        <main className="flex-auto">
          <div className="container mx-auto">{children}</div>
        </main>
        <Footer />
      </section>
    </>
  );
}

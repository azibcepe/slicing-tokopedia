import { useState } from 'react';
import { FaQrcode } from 'react-icons/fa';
import Link from 'next/link';
import Router from 'next/router';

export default function ModalLogin({ toggle }) {
  const [nohp, setNohp] = useState('');
  return (
    <div className="card">
      <div className="card-body p-4">
        <div className="flex justify-between mb-8">
          <h3 className="card-title">Masuk</h3>
          <a
            onClick={(e) => {
              e.preventDefault();
              if (typeof toggle === 'function') toggle();
              Router.push(e.target.href);
            }}
            href="/register"
            className="link link-primary"
          >
            Daftar
          </a>
        </div>
        <div className="mb-4">
          <div>
            <label className="font-bold">Nomor HP atau Email</label>
            <input
              className="input input-bordered w-full"
              type="text"
              name="nohp"
              defaultValue={nohp}
              onChange={(e) => setNohp(e.target.value)}
            />
            <p className="text-gray-500 mt-1">Contoh: email@blahblah.com</p>
          </div>
          <div>
            <div className="text-right mt-4">
              <a href="#forgot" className="link link-primary">
                Lupa Kata Sandi?
              </a>
            </div>
            <div className="text-center mt-2">
              <button
                type="button"
                className="w-full btn btn-primary"
                disabled={nohp.length < 3}
              >
                Selanjutnya
              </button>
            </div>
          </div>
        </div>
        <div className="w-full divider divider-vertical">atau masuk dengan</div>
        <div className="flex flex-col gap-6">
          <button
            type="button"
            className="w-full btn btn-outline btn-ghost btn-bordered"
          >
            Metode Lain
          </button>
          <button
            type="button"
            className="w-full btn btn-outline btn-ghost btn-bordered"
          >
            <FaQrcode className="w-4 h-4 mr-2" /> Scan Kode QR
          </button>
        </div>
        <div className="mt-6 text-center">
          <span>Butuh Bantuan?</span>{' '}
          <a href="#contact" className="link link-primary">
            Hubungi Toko Care
          </a>
        </div>
      </div>
    </div>
  );
}

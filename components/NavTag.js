import { useEffect, useRef, useState } from 'react';
import { useScroll } from 'ahooks';
import { useSelector, useDispatch } from 'react-redux';
import { setCategory } from '@/store/homepageSlice';
import { useGetCategoriesQuery, productApi } from '@/store/productApi';
import clsx from 'clsx';

const gradientColors = [
  'from-purple-500 to-purple-400',
  'from-yellow-500 to-yellow-400',
  'from-green-500 to-green-400',
  'from-red-500 to-red-400',
  'from-pink-500 to-pink-400'
];
const Skeleton = ({ index }) => (
  <div
    className={clsx(
      'opacity-50 flex-none w-48 px-4 py-4 rounded-xl text-white bg-gradient-to-r',
      gradientColors[index] || 'from-blue-400 to-blue-300'
    )}
  >
    {index === 0 && <div className="w-10 h-[3px] bg-white rounded" />}
    <div className="font-bold bg-gray-200 pt-1 h-2 w-20 rounded" />
  </div>
);
const Button = ({ index, text }) => {
  const category = useSelector((state) => state.homepage.category);
  const dispatch = useDispatch();
  return (
    <a
      href={`#`}
      onClick={(e) => {
        e.preventDefault();
        dispatch(setCategory(text));
      }}
      className={clsx(
        'flex-none w-48 px-4 py-4 rounded-xl text-white bg-gradient-to-r',
        gradientColors[index] || 'from-blue-400 to-blue-300'
      )}
    >
      {category == text && <div className="w-10 h-[3px] bg-white rounded" />}
      <div className="font-bold rounded capitalize">
        {text || 'All product'}
      </div>
    </a>
  );
};

export default function NavTag() {
  const { data, isLoading, isError } = useGetCategoriesQuery();
  const [prevTop, setPrevTop] = useState(0);
  const ref = useRef(null);
  const scroll = useScroll();
  useEffect(() => {
    //console.log(productApi);
  }, []);
  useEffect(() => {
    const el = ref.current;
    if (el) {
      let isFixed = el.classList.contains('sticky-nav2');
      if (scroll.top > el.offsetTop && !isFixed) {
        // go fixed if scroll top > el current top
        setPrevTop(el.offsetTop);
        el.classList.add('sticky-nav2');
      }
      if (scroll.top < prevTop && isFixed) {
        // clear fixed
        setPrevTop(0);
        el.classList.remove('sticky-nav2');
      }
    }
  }, [scroll]);
  return (
    <div
      ref={ref}
      className="bg-white flex flex-nowrap items-center w-full max-w-full flex-nowrap gap-2 overflow-x-auto overflow-y-hidden py-2"
    >
      {isLoading || isError
        ? Array(5).map((_, index) => <Skeleton key={index} index={index} />)
        : [null]
            .concat(data)
            .map((i, index) => <Button key={index} index={index} text={i} />)}
    </div>
  );
}

import { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { addCart, removeCart } from '@/store/cart';
import { useGetCategoriesQuery } from '@/store/productApi';
import {
  FaBars,
  FaTimes,
  FaShoppingCart,
  FaSearch,
  FaUser,
  FaTrash
} from 'react-icons/fa';
import { BsPhone } from 'react-icons/bs';
import Link from 'next/link';
import useModal from '@/libs/useModal';
import clsx from 'clsx';
import { preventDefault } from '@/libs/utils';
import LoginModal from '@/components/ModalLogin';
import RegisterModal from '@/components/ModalRegister';

function TopNavbar() {
  return (
    <div className="hidden md:flex px-4 py-1 md:px-10 items-center bg-gray-100 text-base-content justify-between text-sm">
      <div className="inline-flex gap-2">
        <BsPhone className="w-4 h-4" />
        <a onClick={preventDefault} href="#">
          Download Toko App
        </a>
      </div>
      <div className="inline-flex gap-3">
        <a onClick={preventDefault} href="#blah">
          Tentang Toko
        </a>
        <a onClick={preventDefault} href="#blah">
          Mitra Toko
        </a>
        <a onClick={preventDefault} href="#blah">
          Mulai Jualan
        </a>
        <a onClick={preventDefault} href="#blah">
          Toko Care
        </a>
      </div>
    </div>
  );
}
function NavbarCategory({ show = false, className }) {
  const { data, isLoading, isError } = useGetCategoriesQuery();
  const [tabIndex, setTabIndex] = useState(0);
  const tabClass = (n) =>
    clsx('whitespace-nowrap tab h-10 lg:h-14 capitalize', {
      'tab-active tab-bordered': tabIndex === n
    });
  const tabSelect = (n) => (e) => {
    e.preventDefault();
    setTabIndex(n);
  };
  useEffect(() => {
    if (show) {
      document.body.classList.add('overflow-hidden');
    } else {
      document.body.classList.remove('overflow-hidden');
    }
  }, [show]);
  return (
    <div
      className={clsx(
        'absolute w-full h-screen shadow-sm bg-base-100 text-base-content border border-t z-40',
        {
          hidden: !show
        }
      )}
    >
      <ul className="tabs flex-nowrap overflow-x-auto overflow-y-hidden mb-4">
        {isLoading ? (
          <a>Loading...</a>
        ) : isError ? (
          <a>No Category</a>
        ) : (
          data.map((i, index) => {
            return (
              <a
                onClick={tabSelect(index)}
                key={index}
                href={`#category-${i}`}
                className={tabClass(index)}
              >
                {i}
              </a>
            );
          })
        )}
      </ul>
      <div className="p-4 capitalize">Selected {tabIndex}</div>
    </div>
  );
}

export default function Navbar() {
  const cart = useSelector((state) => state.cart);
  const [mounted, setMounted] = useState(false);
  const dispatch = useDispatch();
  const [open, setOpen] = useState(false);
  const ModalLogin = useModal();
  const handleToggle = (e) => {
    e.preventDefault();
    setOpen(!open);
  };
  useEffect(() => setMounted(true), []);
  return (
    <>
      <TopNavbar />
      <div className="navbar md:px-10 shadow-sm bg-base text-base-content">
        <div className="navbar-start gap-2 lg:gap-3">
          <Link href="/">
            <a className="hidden sm:block link link-neutral p-2">
              <img className="w-24 md:w-36" src="/images/tokopedia.svg" />
            </a>
          </Link>
          <a
            href="#"
            onClick={handleToggle}
            className="hidden md:block btn btn-xs btn-ghost rounded-sm  capitalize"
          >
            Kategori
          </a>
        </div>
        <div className="flex-1 mr-4 ml-2 lg:mx-2">
          <div className="relative">
            <input
              type="search"
              placeholder="Pencarian"
              className="xxxxs:w-28 xxxs:w-36 xxs:w-48 xs:w-60 sm:w-96 lg:w-[560px] xl:w-[720px] input input-sm input-bordered"
            />
            <button className="absolute top-0 right-0 btn btn-sm rounded-l-none btn-ghost text-primary">
              <FaSearch className="w-4 h-4" />
            </button>
          </div>
        </div>
        <div className="navbar-end">
          <div className="inline-flex gap-2 lg:gap-3 items-center mr-3 lg:mr-0">
            {/*<button
              onClick={(e) => e.preventDefault()}
              className="lg:hidden btn btn-sm btn-ghost btn-square"
            >
              <FaSearch className="w-4 h-4" />
            </button>*/}
            <div className="dropdown dropdown-end">
              <div
                tabIndex="0"
                className="indicator btn btn-sm btn-ghost btn-square"
              >
                <FaShoppingCart className="w-4 h-4" />
                {mounted && cart.items.length !== 0 && (
                  <div className="indicator-item badge p-1">
                    <span className="text-red-500 text-xs">
                      {cart.items.length}
                    </span>
                  </div>
                )}
              </div>
              <ul
                tabIndex="0"
                className="shadow dropdown-content bg-base-100 rounded-md w-48 p-0"
              >
                {mounted && cart.items.length !== 0 ? (
                  cart.items.map((i, index) => {
                    return (
                      <li
                        key={index}
                        className="flex justify-between items-center p-2"
                      >
                        <a
                          href={`#product${i.id}`}
                          onClick={preventDefault}
                          className="link max-w-[120px] whitespace-nowrap truncate"
                        >
                          {i.title}
                        </a>
                        <button
                          type="button"
                          onClick={(e) => {
                            e.preventDefault();
                            dispatch(removeCart(i.id));
                          }}
                          className="btn btn-circle btn-ghost btn-xs text-red-500"
                        >
                          <FaTrash className="w-3 h-3" />
                        </button>
                      </li>
                    );
                  })
                ) : (
                  <div className="card">
                    <div className="card-body text-center">
                      Your Cart Is Empty
                    </div>
                  </div>
                )}
              </ul>
            </div>
            <hr className="w-[1px] h-6 bg-gray-300" />
            <a
              href="#masuk"
              className="hidden md:block btn btn-sm btn-ghost btn-outline capitalize"
              onClick={(e) => {
                preventDefault(e);
                ModalLogin.toggle();
              }}
            >
              Masuk
            </a>
            <Link href="/register">
              <a
                href="#daftar"
                className="hidden md:block btn btn-sm btn-primary btn-outline capitalize"
              >
                Daftar
              </a>
            </Link>
            <div className="md:hidden dropdown dropdown-end">
              <div tabIndex="0" className="btn btn-sm btn-ghost btn-square">
                <FaUser className="w-4 h-4" />
              </div>
              <ul
                tabIndex="0"
                className="shadow menu dropdown-content bg-base-100 rounded-box w-52 rounded-md"
              >
                <li>
                  <a
                    onClick={(e) => {
                      preventDefault(e);
                      ModalLogin.toggle();
                    }}
                  >
                    Login
                  </a>
                </li>
                <li>
                  <Link href="/register">
                    <a>Register</a>
                  </Link>
                </li>
              </ul>
            </div>
          </div>
          <button
            onClick={handleToggle}
            className="md:hidden btn btn-sm btn-ghost btn-square"
          >
            {open ? (
              <FaTimes className="w-4 h-4" />
            ) : (
              <FaBars className="w-4 h-4" />
            )}
          </button>
        </div>
      </div>
      {/* NavbarCategory */}
      <NavbarCategory show={open} />
      <ModalLogin.Component closeable>
        <LoginModal toggle={ModalLogin.toggle} />
      </ModalLogin.Component>
    </>
  );
}

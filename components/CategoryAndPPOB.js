import { useState, useEffect } from 'react';
import Carousel from '@/components/Carousel';
import { preventDefault } from '@/libs/utils';
import clsx from 'clsx';

function CarouselCategoryProduct() {
  return (
    <Carousel
      slidesPerView={4}
      navigation
      items={Array(10)
        .fill('')
        .map((i, index) => {
          return (
            <div className="w-24 h-24 border rounded shadow cursor-pointer text-center">
              Cat {index}
            </div>
          );
        })}
    />
  );
}

function FormPPOB() {
  const [nohp, setNohp] = useState('');
  const [nom, setNom] = useState('');
  const [tabIndex, setTabIndex] = useState(0);
  const tabClass = (i) =>
    clsx('tab whitespace-nowrap', {
      'tab-active tab-bordered': tabIndex === i
    });
  const selectTab = (n) => (e) => {
    e.preventDefault();
    setTabIndex(n);
  };

  // useEffect(() => {
  //   console.log(nohp)
  // }, [nohp])

  return (
    <div className="card border shadow rounded-md">
      <div className="card-body p-0">
        <nav className="tabs mb-4 overflow-x-auto overflow-y-hidden justify-between border-b flex-nowrap p-2">
          <a href="#" onClick={selectTab(0)} className={tabClass(0)}>
            Pulsa
          </a>
          <a href="#" onClick={selectTab(1)} className={tabClass(1)}>
            Paket Data
          </a>
          <a href="#" onClick={selectTab(2)} className={tabClass(2)}>
            Listrik PLN
          </a>
          <a href="#" onClick={selectTab(3)} className={tabClass(3)}>
            Flight
          </a>
          <a href="#" onClick={selectTab(4)} className={tabClass(4)}>
            Lainya...
          </a>
        </nav>
        <div className="tab-container">
          <div
            className={clsx('p-4', {
              hidden: tabIndex === 0
            })}
          >
            PPOB Tab Index {tabIndex}
          </div>
          <div
            className={clsx('flex justify-between p-4 gap-2', {
              hidden: tabIndex !== 0
            })}
          >
            <div className="w-5/12">
              <input
                className="w-full input input-sm input-bordered"
                placeholder="Contoh. 08123456789"
                onChange={(e) => setNohp(e.target.value)}
                defaultValue={nohp}
                name="nohp"
                type="text"
              />
            </div>
            <div className="w-5/12">
              <select
                className="w-full select select-sm select-bordered"
                name="nom"
                onChange={(e) => setNom(e.target.value)}
                disabled={nohp.length < 10}
              >
                <option readOnly defaultValue={nom}>
                  Pilih Nominal
                </option>
                <option value="">Rp. 10.000</option>
                <option value="">Rp. 20.000</option>
              </select>
            </div>
            <div>
              <button
                className="w-full btn btn-sm btn-success"
                type="burton"
                disabled={nohp.length < 10}
              >
                Beli
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

function BtnListPill() {
  return (
    <div className="w-full inline-flex gap-3 overflow-x-auto overflow-y-hidden p-3 flex-nowrap">
      {Array(12)
        .fill('Tag ')
        .map((i, index) => {
          return (
            <a
              href="#"
              onClick={preventDefault}
              key={index}
              className="btn btn-sm btn-ghost capitalize shadow rounded-xl px-6 text-sm font-light"
            >
              {i} {index}
            </a>
          );
        })}
    </div>
  );
}

export default function CategoryAndPPOB() {
  return (
    <div className="card border shadow-md rounded-md mt-4">
      <div className="card-body p-4">
        <div className="flex flex-col lg:flex-row gap-4">
          <div className="w-full lg:w-6/12">
            <h3 className="card-title lg:text-2xl">Kategori Pilihan</h3>
            <CarouselCategoryProduct />
          </div>
          <div className="w-full lg:w-6/12">
            <h3 className="card-title lg:text-2xl">Topup & Tagihan</h3>
            <FormPPOB />
          </div>
        </div>
        <div className="w-full mt-4">
          <BtnListPill />
        </div>
      </div>
    </div>
  );
}

import { useEffect, useRef } from 'react';
import { useSetState, useSet } from 'ahooks';
import { useSelector } from 'react-redux';
import { useGetProductsQuery } from '@/store/productApi';
import ProductCard from '@/components/ProductCard';
import NavTag from '@/components/NavTag';

export function Loading() {
  return <div>Loading...</div>;
}
export function LoadingError() {
  return <div>Error while loading data</div>;
}

export default function Products() {
  const category = useSelector((state) => state.homepage.category);
  const { data, error, isError, isLoading } = useGetProductsQuery({ category });

  if (isLoading) return <Loading />;
  if (isError) return <LoadingError />;

  return data.length !== 0 ? (
    <>
      <div className="my-5">
        <NavTag />
      </div>
      <div className="mt-6 p-2 lg:p-0">
        <div className="grid grid-cols-2 sm:grid-cols-3 md:grid-cols-4 lg:grid-cols-5 xl:grid-cols-6 gap-2">
          {data.map((i, index) => (
            <ProductCard key={i.id} data={i} />
          ))}
        </div>
      </div>
    </>
  ) : (
    <div>No data.</div>
  );
}

import { useState, useEffect, Children } from 'react';
import PropTypes from 'prop-types';

export function Tab() {
  return <div className="tab"></div>;
}
export function Tabs({ children }) {
  // ignore children if not a Tab
  children = Children.toArray(children).filter(
    (i) => typeof i.type === 'function' && i.type === Tab
  );
  console.log(children); 
  return <div className="tabs"></div>;
}
 
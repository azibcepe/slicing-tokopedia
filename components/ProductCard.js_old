import { useDispatch, useSelector } from 'react-redux';
import { addCart, removeCart } from '@/store/cart';
import { preventDefault } from '@/libs/utils';
import { FaStar, FaShoppingCart } from 'react-icons/fa';
import { FcApproval, FcApprove, FcBusinessman } from 'react-icons/fc';
import { formatAngka } from '@/libs/utils';

const SellerTypeIcon = ({ status }) => {
  const props = { className: 'w-5 h-5' };
  if (status === 2) return <FcApprove {...props} />;
  if (status === 3) return <FcApproval {...props} />;
  return <FcBusinessman {...props} />;
};

export default function ProductCard({ id, data }) {
  const cart = useSelector(state => state.cart);
  const dispatch = useDispatch();
  return (
    <div id={id} className="card rounded-md shadow border cursor-pointer bg-white">
      <a
        href={`#product${data.id}`}
        onClick={preventDefault}
        className="w-full"
      >
        <img src={data.thumbnail} className="rounded-t-md w-full" />
        {data.isHot && (
          <div className="whitespace-nowrap text-white font-bold text-xs text-center w-14 pr-2 pt-[2px] h-5 bg-yellow-500 rounded-l rounded-full">
            Terlaris
          </div>
        )}
      </a>
      <div className="relative card-body p-2 gap-y-1">
        <a
          href={`#product${data.id}`}
          onClick={preventDefault}
          className="hover:underline"
        >
          <h3 className="text-sm">{data.product_name}</h3>
        </a>
        <div className="inline-flex justify-between items-center">
          <div className="font-black">{data.price}</div>
          <button
            type="button"
            className="btn btn-sm btn-ghost btn-circle text-green-800"
            onClick={(e) => {
              e.preventDefault();
              dispatch(addCart(data))
            }}
          >
            <FaShoppingCart className="w-4 h-4" />
          </button>
        </div>
        {data.cashback && (
          <p className="my-1 p-[1px] text-xs bg-green-300 w-16 rounded text-green-700 text-center">
            Cashback
          </p>
        )}
        <div className="inline-flex items-center text-sm text-gray-500 gap-1">
          <SellerTypeIcon status={data.sellerType} />
          <div>{data.city}</div>
        </div>
        <div className="inline-flex items-center text-[13px] text-gray-500 gap-1 h-6">
          <span className="inline-flex text-center items-baseline">
            <FaStar className="w-4 mr-[1px] text-yellow-500" />
            {data.rating}
          </span>
          <hr className="w-[1px] h-4 bg-gray-300" />
          <span>Terjual {formatAngka(data.sold)}</span>
        </div>
      </div>
    </div>
  );
}

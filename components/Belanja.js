import { useEffect, useState } from 'react';
import Link from 'next/link';
import clsx from 'clsx';
import { FaPen } from 'react-icons/fa';
import { useToggle, useCounter } from 'ahooks';

let stok = 80;

export default function Belanja({ data }) {
  const [totalAmount, setTotalAmount] = useState(0);
  const [counterValue, counterMethod] = useCounter(0, {
    min: 1,
    max: stok
  });
  useEffect(() => {
    if (counterValue > 0)
      setTotalAmount(counterValue * data.price);
  }, [counterValue]);
  const [noteShow, { toggle: toggleNote }] = useToggle(false);
  return (
    <>
      <div className="inline-flex gap-4 items-center">
        <div className="relative w-32">
          <button
            type="button"
            onClick={(e) => counterMethod.dec()}
            className="absolute top-0 left-0 rounded-r-none btn btn-primary btn-outline border-none text-xl"
          >
            -
          </button>
          <input
            type="number"
            className="input input-primary input-bordered w-full text-center text-xl"
            value={counterValue}
            onChange={(e) => counterMethod.set((c) => e.target.value)}
          />
          <button
            type="button"
            onClick={(e) => counterMethod.inc()}
            className="absolute top-0 right-0 rounded-l-none btn btn-primary btn-outline border-none text-xl"
          >
            +
          </button>
        </div>
        <strong>Sisa Stok: {stok - counterValue}</strong>
      </div>
      <div className="mt-5">
        {noteShow && (
          <div className="mb-2">
            <input
              type="text"
              className="w-full input input-accent"
              placeholder="Contoh: Warna, Ukuran, dll."
            />
          </div>
        )}
        <a
          href="#"
          className="inline-flex gap-2 items-center text-primary font-bold"
          onClick={(e) => (e.preventDefault(), toggleNote())}
        >
          {noteShow ? (
            <span>Tutup Catatan</span>
          ) : (
            <>
              <FaPen className="w-4 h-4" />
              <span>Tambah Catatan</span>
            </>
          )}
        </a>
      </div>
      <div className="inline-flex justify-between items-center mt-5">
        <span className="text-gray-500">Subtotal</span>
        <strong className="text-2xl">$ {totalAmount.toLocaleString('en-US')}</strong>
      </div>
      <div className="flex flex-col gap-4 mt-5 w-full">
        <button type="button" className="btn w-full btn-primary">
          + Keranjang
        </button>
        <button type="button" className="btn w-full btn-primary btn-outline">
          Beli
        </button>
      </div>
    </>
  );
}

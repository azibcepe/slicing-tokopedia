import { useState, useEffect } from 'react';
import { useCountDown, useSetState, useResponsive } from 'ahooks';
import { preventDefault } from '@/libs/utils';
import ProductCard from '@/components/ProductCard';
import { useGetProductsQuery } from '@/store/productApi';
import clsx from 'clsx';

export default function FlashSale() {
  const screen = useResponsive();
  const [mounted, setMounted] = useState(false);
  const { data, isError, isLoading } = useGetProductsQuery({
    limit: 7,
    sort: 'asc'
  });
  const [countdown, setTargetDate, formattedRes] = useCountDown({
    targetDate: new Date(Date.now() + 1 * 9 * 3600 * 1000).toUTCString()
  });
  const { days, hours, minutes, seconds, milliseconds } = formattedRes;
  useEffect(() => setMounted(true), []);
  return (
    <div className="my-10">
      <div className="p-2 flex flex-col md:flex-row md:gap-4 md:items-center">
        <h3 className="font-black text-2xl">Flash Sale Demo</h3>
        {mounted ? (
          <div className="inline-flex gap-2 text-md font-bold text-gray-500 items-center">
            <div>Berakhir dalam:</div>
            <div className="flex gap-1 items-center">
              <div className="countdown bg-red-600 text-white p-2 rounded">
                <span style={{ '--value': hours }} />
              </div>
              <div className="text-red-600">:</div>
              <div className="countdown bg-red-600 text-white p-2 rounded">
                <span style={{ '--value': minutes }} />
              </div>
              <div className="text-red-600">:</div>
              <div className="countdown bg-red-600 text-white p-2 rounded">
                <span style={{ '--value': seconds }} />
              </div>
            </div>
          </div>
        ) : (
          <div>Loading countdown...</div>
        )}
        <a
          className="link text-primary font-bold"
          href="#lihatsemua"
          onClick={preventDefault}
        >
          Lihat Semua
        </a>
      </div>
      <div className="relative p-2">
        <a
          href="#"
          onClick={preventDefault}
          className="flex justify-center w-56 rounded-md"
          style={{ backgroundColor: '#049642' }}
        >
          <img
            className="w-28"
            src="/images/293a299e-4b2e-4198-bff5-681c558a8efd.png.webp"
          />
        </a>
        {mounted &&
          (isError ? (
            <div>Error loading product</div>
          ) : isLoading ? (
            <div>Product loading</div>
          ) : (
            <div
              className="absolute top-0 whitespace-nowrap overflow-auto rounded-md"
              style={{
                left: !screen.md ? 0 : '180px',
                maxWidth: !screen.md ? '100%' : 'calc(100% - 180px)'
              }}
            >
              <div className="inline-flex gap-2">
                {data.map((i, index) => (
                  <div key={i.id} className="w-40 h-80">
                    <ProductCard id={`index${i}`} data={i} />
                  </div>
                ))}
              </div>
            </div>
          ))}
      </div>
    </div>
  );
}

export default function FloatingIcon() {
  return (
    <a
      title="Ambil hadiahmu"
      href="#"
      className="fixed bottom-1 right-1 lg:bottom-10 lg:right-10 z-50 p-2"
      style={{
        color: '#0C9715'
      }}
    >
      <img src="/images/FLOATING-ICON-JANUARY-150x150-1.gif" className="w-20" />
    </a>
  );
}

import { useState } from 'react';
import { preventDefault } from '@/libs/utils';
import { FaGooglePlay, FaApple } from 'react-icons/fa';
import FloatingIcon from '@/components/FloatingIcon';
import clsx from 'clsx';

export default function Footer() {
  const [tabIndex, setTabIndex] = useState(0);
  const tabClass = (n) =>
    clsx('whitespace-nowrap tab h-10 lg:h-16', {
      'tab-active tab-bordered': tabIndex === n
    });
  const tabSelect = (n) => (e) => {
    e.preventDefault();
    setTabIndex(n);
  };
  return (
    <footer className="container mx-auto px-2">
      <div className="mt-4">
        <h3 className="text-xl font-black">Cari Semua Di toko!</h3>
        <ul className="tabs flex-nowrap overflow-x-auto overflow-y-hidden my-2 justify-between border-b">
          {Array(10)
            .fill(null)
            .map((i, index) => {
              return (
                <a
                  onClick={tabSelect(index)}
                  key={index}
                  href={`#category-${index}`}
                  className={tabClass(index)}
                >
                  Tab {index}
                </a>
              );
            })}
        </ul>
        <div className="tab-tab-tab mt-2 px-2">
          <ul>
            {Array(3)
              .fill(null)
              .map((i, index) => {
                return (
                  <li key={index} className="border-b py-1">
                    <a
                      href="#"
                      className="link no-underline"
                      onClick={preventDefault}
                    >
                      List {index} Tab {tabIndex}
                    </a>
                  </li>
                );
              })}
          </ul>
        </div>
      </div>
      <div className="border-t border-b mt-4 grid grid-cols-1 sm:grid-cols-2 gap-4 py-4">
        <div className="flex flex-col gap-6">
          <h4 className="font-black text-yellow-500">
            Punya Toko Online? Buka Cabangnya Disini
          </h4>
          <p>
            Mudah, nyaman dan bebas komisi transaksi.{' '}
            <strong className="font-bold">GRATIS!</strong>
          </p>
          <div className="inline-flex gap-4 items-center">
            <button type="button" className="btn btn-primary">
              Buka Toko GRATIS
            </button>
            <a
              href="#"
              onClick={preventDefault}
              className="text-primary font-bold"
            >
              Pelajari lebih lanjut.
            </a>
          </div>
        </div>
        <div>
          <img src="/images/SEOcontent1.jpg" />
        </div>
      </div>
      <div className="mt-4">
        <h3>Nikmati mudahnya belanja di toko kami</h3>
        <p className="text-xs">
          Tokopedia merupakan salah satu situs jual beli online di Indonesia
          yang perkembangannya terhitung cepat dan memiliki tujuan untuk
          memudahkan setiap masyarakat di Indonesia, agar dapat melakukan aneka
          transaksi jual beli secara online. Selain kamu dapat menikmati proses
          pembelian aneka produk lebih mudah dan efisien, kamu para seller juga
          dapat melakukan jualan online di Tokopedia. Kamu bisa bergabung dengan
          komunitas khusus Tokopedia Seller bagi kamu yang ingin memulai bisnis
          dan jualan online atau ingin memperluas bisnis yang sedang kamu
          jalankan. Proses pendaftaran untuk menjadi Tokopedia Seller juga
          sangat mudah cukup dengan memasukkan data diri, nama toko, alamat toko
          setelah itu kamu akan langsung terdaftar sebagai Tokopedia Seller.
          Kamu juga dapat melakukan upgrade akun toko kamu menjadi Power
          Merchant untuk menjangkau pelanggan Tokopedia yang lebih luas lagi,
          sehingga bisnis online kamu semakin laris. Keuntungan Power Merchant
          adalah kamu dapat memberikan fitur Bebas Ongkir sehingga dapat menarik
          lebih banyak lagi pelanggan, lalu kamu dapat menikmati fitur TopAds
          yang dapat menjangkau masyarakat pengguna Tokopedia lebih banyak lagi
          dengan modal yang sangat minim mulai dari Rp 25 ribuan, hingga toko
          kamu akan tampil lebih menarik lagi serta dapat meningkatkan
          kepercayaan pembeli. Ayo mulai jualan online di Tokopedia dan mulai
          kembangkan usahamu secara online bersama Tokopedia. <br /> <br />{' '}
          <a className="link link-primary" href="#more">
            Selengkapnya.
          </a>
        </p>
        <div className="mt-8 grid grid-cols-3 gap-6">
          <a href="#" className="flex gap-4">
            <div className="hidden md:block">
              <img src="/images/SEOcontent2.jpg" className="w-32" />
            </div>
            <div>
              <h4 className="text-primary font-bold mb-3">Transparan</h4>
              <p>
                Pembayaran Anda baru diteruskan ke penjual setelah barang Anda
                terima
              </p>
            </div>
          </a>
          <a href="#" className="flex gap-4">
            <div className="hidden md:block">
              <img src="/images/SEOcontent3.jpg" className="w-32" />
            </div>
            <div>
              <h4 className="text-primary font-bold mb-3">Aman</h4>
              <p>
                Bandingkan review untuk berbagai online shop terpercaya
                se-Indonesia
              </p>
            </div>
          </a>
          <a href="#" className="flex gap-4">
            <div className="hidden md:block">
              <img src="/images/SEOcontent3.jpg" className="w-32" />
            </div>
            <div>
              <h4 className="text-primary font-bold mb-3">
                Fasilitas Escrow Gratis
              </h4>
              <p>
                Fasilitas Escrow (Rekening Bersama) Tokopedia tidak dikenakan
                biaya tambahan
              </p>
            </div>
          </a>
        </div>
      </div>
      <h3 className="mt-20 text-xl font-black">Footer Links</h3>
      <div className="mt-8 mb-3 lg:mb-10 grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-6">
        <ul>
          {Array(3)
            .fill(null)
            .map((i, index) => {
              return (
                <li key={index} className="border-b py-1">
                  <a
                    href="#"
                    className="link no-underline"
                    onClick={preventDefault}
                  >
                    Foo Bar {index}
                  </a>
                </li>
              );
            })}
        </ul>
        <ul>
          {Array(3)
            .fill(null)
            .map((i, index) => {
              return (
                <li key={index} className="border-b py-1">
                  <a
                    href="#"
                    className="link no-underline"
                    onClick={preventDefault}
                  >
                    Foo Bas {index}
                  </a>
                </li>
              );
            })}
        </ul>
        <div className="text-center">
          <figure>
            <img src="/images/il-footer-2.jpg" />
          </figure>
          <div className="flex gap-4 justify-center mt-4">
            <button className="btn rounded" type="button">
              <FaGooglePlay className="w-4 h-4 mr-2" /> G-Play
            </button>
            <button className="btn rounded" type="button">
              <FaApple className="w-4 h-4 mr-2" /> APP STORE
            </button>
          </div>
          <div className="mt-4">&copy; 2021 PT. Alim Rugi</div>
        </div>
      </div>
      <FloatingIcon />
    </footer>
  );
}
